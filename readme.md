# A Fresh Start

I always admire pristine dotfiles repositories that seem to have been crafted with a level of intentionality, order, and maintainability I'd like to strive for. Unfortunately, life can be messy, especially when you're going in too many directions, and my dotfiles have become haphazard and frayed.

It's time to change that. This repository is a new beginning and a look toward the future I am working hard to build. As such, it's incomplete and in a state of liminality. There will be mistakes and challenges; I don't recommend looking to this repository as a point of reference or inspiration for the time being.

## Architecture 

The first step is deciding how we're going to manage this thing. Symlinks are fun but very easy to lose control of. Instead, I'm going to try using [Chezmoi](https://www.chezmoi.io/) to help impose more structure and consistency.

Chezmoi has a few really powerful advantages:

- **Idempotence**: It enforces consistency and a deliberate and declarative methodology. This is particularly good for managing ADHD tendencies.  
- **Templating**: By using Go's templating library, it's easy to write dotfiles that adapt to whatever system or environment I'm using. I've found myself using Linux more and more recently, so being able to write adaptable dotfiles for macOS, Linux, and whatever context I'm running them on is a fantastic addition. 
- **Security**: One of the things that really sets Chezmoi apart is built-in support for a ton of password managers. I'm using [1Password](https://1password.com/), and integration with their CLI makes secret management a breeze. Between [op](https://developer.1password.com/docs/cli/get-started/) and 1Password's [SSH agent](https://developer.1password.com/docs/ssh/agent/security/), managing secure information in my environment has become a lot less painful than it used to be.
- **Fast**: It's very easy to bootstrap a new system with Chezmoi, and I plan to integrate Ansible with it for rapid environment setup and management. 

## Roadmap

The first few items I'll be working on are:

- My [Neovim](https://neovim.io/) config, which I'm in the process of writing from scratch after spending a fair bit of time using kickstart.nvim.
- My [Wezterm](https://wezfurlong.org/wezterm/index.html) configuration. A while back, I switched from iTerm2 to Warp. I like its speed and features, but I'm less enthusiastic about it being closed-source, and their complete [disregard for ligatures support](https://github.com/warpdotdev/warp/issues/239) left me wanting something, ahem, actually modern. Enter Wezterm: it's on par with Warp or Kitty's speed, and everything can be customized. It's got a Lua config, the same as Neovim, so between the two, I've been getting pretty comfortable with Lua.
- My ZSH and shell setup. When macOS switched from Bash to ZSH, I let most of my shell configuration languish. While ZSH is pretty good out of the box, I'd like to make it my own.
